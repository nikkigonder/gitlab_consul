override['consul']['version'] = '1.0.3'

default['gitlab_consul']['agent']['enabled'] = true
default['gitlab_consul']['agent']['shell'] = '/bin/false'

default['gitlab_consul']['cluster_nodes'] = ["null"]

default['gitlab_consul']['client']['bind_interface'] = nil

default['gitlab_consul']['client']['tls']['ssl_key']['path'] = '/etc/consul/ssl/private/consul.key'
default['gitlab_consul']['client']['tls']['ssl_key']['source'] = 'attribute'
default['gitlab_consul']['client']['tls']['ssl_key']['secret_source'] = 'chef_vault'
default['gitlab_consul']['client']['tls']['ssl_key']['secret_bag'] = 'gitlab_consul'
default['gitlab_consul']['client']['tls']['ssl_key']['item'] = 'client'
default['gitlab_consul']['client']['tls']['ssl_key']['item_key'] = 'private_key'
default['gitlab_consul']['client']['tls']['ssl_key']['content'] = ''
default['gitlab_consul']['client']['tls']['ssl_cert']['path'] = '/etc/consul/ssl/certs/consul.crt'
default['gitlab_consul']['client']['tls']['ssl_cert']['source'] = 'attribute'
default['gitlab_consul']['client']['tls']['ssl_cert']['secret_source'] = 'chef_vault'
default['gitlab_consul']['client']['tls']['ssl_cert']['secret_bag'] = 'gitlab_consul'
default['gitlab_consul']['client']['tls']['ssl_cert']['item'] = 'client'
default['gitlab_consul']['client']['tls']['ssl_cert']['item_key'] = 'certificate'
default['gitlab_consul']['client']['tls']['ssl_cert']['content'] = ''
default['gitlab_consul']['client']['tls']['ssl_chain']['name'] = 'consul-chain.crt'
default['gitlab_consul']['client']['tls']['ssl_chain']['path'] = '/etc/consul/ssl/certs/chain.crt'
default['gitlab_consul']['client']['tls']['ssl_chain']['name'] = 'chain.crt'
default['gitlab_consul']['client']['tls']['ssl_chain']['source'] = 'attribute'
default['gitlab_consul']['client']['tls']['ssl_chain']['secret_source'] = 'chef_vault'
default['gitlab_consul']['client']['tls']['ssl_chain']['secret_bag'] = 'gitlab_consul'
default['gitlab_consul']['client']['tls']['ssl_chain']['item'] = 'client'
default['gitlab_consul']['client']['tls']['ssl_chain']['item_key'] = 'ca_certificate'
default['gitlab_consul']['client']['tls']['ssl_chain']['content'] = ''

default['gitlab_consul']['cluster']['bind_interface'] = nil

default['gitlab_consul']['cluster']['tls']['ssl_key']['path'] = '/etc/consul/ssl/private/consul.key'
default['gitlab_consul']['cluster']['tls']['ssl_key']['source'] = 'attribute'
default['gitlab_consul']['cluster']['tls']['ssl_key']['secret_source'] = 'chef_vault'
default['gitlab_consul']['cluster']['tls']['ssl_key']['secret_bag'] = 'gitlab_consul'
default['gitlab_consul']['cluster']['tls']['ssl_key']['item'] = 'cluster'
default['gitlab_consul']['cluster']['tls']['ssl_key']['item_key'] = 'private_key'
default['gitlab_consul']['cluster']['tls']['ssl_key']['content'] = ''
default['gitlab_consul']['cluster']['tls']['ssl_cert']['path'] = '/etc/consul/ssl/certs/consul.crt'
default['gitlab_consul']['cluster']['tls']['ssl_cert']['source'] = 'attribute'
default['gitlab_consul']['cluster']['tls']['ssl_cert']['secret_source'] = 'chef_vault'
default['gitlab_consul']['cluster']['tls']['ssl_cert']['secret_bag'] = 'gitlab_consul'
default['gitlab_consul']['cluster']['tls']['ssl_cert']['item'] = 'cluster'
default['gitlab_consul']['cluster']['tls']['ssl_cert']['item_key'] = 'certificate'
default['gitlab_consul']['cluster']['tls']['ssl_cert']['content'] = ''
default['gitlab_consul']['cluster']['tls']['ssl_chain']['name'] = 'consul-chain.crt'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['path'] = '/etc/consul/ssl/certs/chain.crt'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['name'] = 'chain.crt'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['source'] = 'attribute'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['secret_source'] = 'chef_vault'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['secret_bag'] = 'gitlab_consul'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['item'] = 'cluster'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['item_key'] = 'ca_certificate'
default['gitlab_consul']['cluster']['tls']['ssl_chain']['content'] = ''

default['gitlab_consul']['services'] = []
