node['gitlab_consul']['services'].each do |service|
  consul_definition service['name'] do
    type 'service'
    parameters(
        port:  service['port'],
        address: service['address'],
        tags: service['tags'],
        check: service['check']
    )
  end
end