name 'gitlab_consul'
maintainer 'John Northrup'
maintainer_email 'john@gitlab.com'
license 'MIT'
description 'Installs/Configures consul server and clients'
long_description 'Installs/Configures consul server and clients'
version '0.1.29'
issues_url 'https://gitlab.com/gitlab-cookbooks/gitlab_consul/issues' if respond_to?(:issues_url)
source_url 'https://gitlab.com/gitlab-cookbooks/gitlab_consul' if respond_to?(:source_url)

depends 'consul', '~> 3.0.0'
depends 'ssl_certificate', '~> 2.1.0'
depends 'poise-service', '~> 1.5.2'
depends 'chef-vault', '~> 3.0'
depends 'gitlab_secrets', '~> 0.0'
