require_relative '../../libraries/address_detector'

describe AddressDetector do
  subject { AddressDetector.new(node, scope) }
  let(:scope) { 'cluster' }
  let(:node) do
    {
      'network' => {
        'interfaces' => {
          'lo' => {
            'encapsulation' => 'Loopback',
            'addresses' => {
              '127.0.0.1' => {
                'family' => 'inet'
              },
              '::1' => {
                'family' => 'inet6'
              }
            }
          },
          'eth0' => {
            'encapsulation' => 'Ethernet',
            'addresses' => {
              'AA:BB:CC:11:22:33': {
                'family' => 'lladdr'
              },
              '192.168.0.2' => {
                'family' => 'inet'
              },
              '::2' => {
                'family' => 'inet6'
              }
            }
          },
          'eth1' => {
            'encapsulation' => 'Ethernet',
            'addresses' => {
              'DD:EE:FF:44:55:66': {
                'family' => 'lladdr'
              },
              '172.16.0.3' => {
                'family' => 'inet'
              },
              '::3' => {
                'family' => 'inet6'
              }
            }
          }
        }
      }
    }
  end

  describe '#ipaddress' do
    context 'when scope is invalid' do
      let(:scope) { 'test_scope' }

      it 'should raise an error' do
        expect { subject }.to raise_error "Unkown scope 'test_scope'. Expected 'client' or 'cluster'"
      end
    end

    context 'when bind_interface is not set' do
      it 'should not raise an error' do
        expect { subject }.not_to raise_error
      end

      it 'should return first inet address of first Ethernet interface' do
        expect(subject.ipaddress).to eq '192.168.0.2'
      end
    end

    context 'when bind_interface is set' do
      before do
        set_bind_interface('eth1')
      end

      it 'should not raise an error' do
        expect { subject }.not_to raise_error
      end

      it 'should return first inet address of selected interface' do
        expect(subject.ipaddress).to eq '172.16.0.3'
      end
    end

    context 'when unexisting interface is set as bind_interface' do
      before do
        set_bind_interface('ens4')
      end

      it 'should raise an exception' do
        expect { subject.ipaddress }.to raise_error "Interface 'ens4' doesn't exists"
      end
    end
  end

  def set_bind_interface(interface)
    node['gitlab_consul'] ||= {}
    node['gitlab_consul'][scope] ||= {}
    node['gitlab_consul'][scope]['bind_interface'] ||= interface
  end
end
